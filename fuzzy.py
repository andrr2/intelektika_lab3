import numpy as np
import matplotlib.pyplot as plt


class FuzzyNumber:
    def __init__(self, number: float):
        assert 0. <= number <= 1., f'Invalid value {number}'

        self.number = number

    def __and__(self, other):
        return FuzzyNumber(min(self.number, other.number))

    def __invert__(self):
        return FuzzyNumber(1 - self.number)

    def __or__(self, other):
        return FuzzyNumber(max(self.number, other.number))

    def __str__(self):
        return str(self.number)


def build_functions():
    def check(x):
        assert 0 <= x <= 1, f'Invalid value {x}'

    def sg_short(x):
        check(x)

        if x < .25:
            value = 1
        elif x < .4:
            value = (.4 - x) / .15
        else:
            value = 0

        return min(value, 1)

    def sg_average(x):
        check(x)

        if .3 <= x < .35:
            value = (x - .3) / .05
        elif .35 <= x < .6:
            value = (.6 - x) / .25
        else:
            value = 0

        return min(value, 1)

    def sg_long(x):
        check(x)

        if x < .45:
            value = 0
        elif x < .5:
            value = (x - .45) / .05
        else:
            value = 1

        return min(value, 1)

    def rg_few(x):
        check(x)

        if x < .2:
            value = 1
        elif x < .3:
            value = (.3 - x) / .1
        else:
            value = 0

        return min(value, 1)

    def rg_average(x):
        check(x)

        if .2 <= x < .3:
            value = (x - .2) / .1
        elif .3 <= x < .5:
            value = (.5 - x) / .2
        else:
            value = 0

        return min(value, 1)

    def rg_many(x):
        check(x)

        if x < .4:
            value = 0
        elif x < .5:
            value = (x - .4) / .1
        else:
            value = 1

        return min(value, 1)

    def sr_short(x):
        check(x)

        if x < .3:
            value = 1
        elif x < .4:
            value = (.4 - x) / .1
        else:
            value = 0

        return min(value, 1)

    def sr_average(x):
        check(x)

        if .3 <= x < .5:
            value = (x - .3) / .2
        elif .5 <= x < .7:
            value = (.7 - x) / .2
        else:
            value = 0

        return min(value, 1)

    def sr_long(x):
        check(x)

        if x < .5:
            value = 0
        elif x < .7:
            value = (x - .5) / .2
        else:
            value = 1

        return min(value, 1)

    def pr_low(x):
        check(x)

        if x < .25:
            value = 1
        elif x < .4:
            value = (.4 - x) / .15
        else:
            value = 0

        return min(value, 1)

    def pr_average(x):
        check(x)

        if .3 <= x < .35:
            value = (x - .3) / .05
        elif .35 <= x < .7:
            value = (.7 - x) / .35
        else:
            value = 0

        return min(value, 1)

    def pr_high(x):
        check(x)

        if x < .5:
            value = 0
        elif x < .65:
            value = (x - .5) / .15
        else:
            value = 1

        return min(value, 1)

    def pg_low(x):
        check(x)

        if x < .25:
            value = 1
        elif x < .4:
            value = (.4 - x) / .15
        else:
            value = 0

        return min(value, 1)

    def pg_average(x):
        check(x)

        if .25 <= x < .5:
            value = (x - .25) / .25
        elif .5 <= x < .65:
            value = (.65 - x) / .15
        else:
            value = 0

        return min(value, 1)

    def pg_high(x):
        check(x)

        if x < .5:
            value = 0
        elif x < .65:
            value = (x - .5) / .15
        else:
            value = 1

        return min(value, 1)

    def kl_very_low(x, max=1):
        check(x)
        check(max)

        if x < .1:
            value = 1
        elif x < .3:
            value = (.3 - x) / .2
        else:
            value = 0

        return min(value, max)

    def kl_low(x, max=1):
        check(x)
        check(max)

        if .1 <= x < .3:
            value = (x - .1) / .2
        elif .3 <= x < .5:
            value = (.5 - x) / .2
        else:
            value = 0

        return min(value, max)

    def kl_middle(x, max=1):
        check(x)
        check(max)

        if .3 <= x < .5:
            value = (x - .3) / .2
        elif .5 <= x < .8:
            value = (.8 - x) / .3
        else:
            value = 0

        return min(value, max)

    def kl_high(x, max=1):
        check(x)
        check(max)

        if x < .6:
            value = 0
        elif x < .8:
            value = (x - .6) / .2
        else:
            value = 1

        return min(value, max)

    return {
        'study_goal': {
            'short': sg_short,
            'average': sg_average,
            'long': sg_long
        },
        'repetition_goal': {
            'few': rg_few,
            'average': rg_average,
            'many': rg_many
        },
        'study_related': {
            'short': sr_short,
            'average': sr_average,
            'long': sr_long
        },
        'performance_related': {
            'low': pr_low,
            'average': pr_average,
            'high': pr_high
        },
        'performance_goal': {
            'low': pg_low,
            'average': pg_average,
            'high': pg_high
        },
        'knowledge_level': {
            'very_low': kl_very_low,
            'low': kl_low,
            'middle': kl_middle,
            'high': kl_high
        }
    }


functions = build_functions()


class Rule:
    def __init__(self, params, rule):
        self.params = params
        self.rule = rule

    def apply(self, args):
        ops = [{'field': param['field'],
                'operation': functions[param['field']][param['function']]}
               for param in self.params]

        return self.rule(*[FuzzyNumber(op['operation'](args[op['field']])) for op in ops]).number


def build_rules():
    return {
        'high': [
            Rule(**{
                'params': [
                    {
                        'field': 'performance_goal',
                        'function': 'high'
                    }
                ],
                'rule': lambda x: x
            }),
            Rule(**{
                'params': [
                    {
                        'field': 'performance_related',
                        'function': 'high'
                    },
                    {
                        'field': 'performance_goal',
                        'function': 'average'
                    }
                ],
                'rule': lambda x, y: x & y
            })
        ],
        'middle': [
            Rule(**{
                'params': [
                    {
                        'field': 'study_goal',
                        'function': 'long'
                    },
                    {
                        'field': 'study_related',
                        'function': 'long'
                    }
                ],
                'rule': lambda x, y: x & y
            }),
            Rule(**{
                'params': [
                    {
                        'field': 'performance_related',
                        'function': 'average'
                    },
                    {
                        'field': 'performance_goal',
                        'function': 'average'
                    }
                ],
                'rule': lambda x, y: x | y
            })
        ],
        'low': [
            Rule(**{
                'params': [
                    {
                        'field': 'performance_goal',
                        'function': 'low'
                    },
                    {
                        'field': 'study_goal',
                        'function': 'short'
                    }
                ],
                'rule': lambda x, y: x & (~ y)
            }),
            Rule(**{
                'params': [
                    {
                        'field': 'performance_related',
                        'function': 'low'
                    },
                    {
                        'field': 'study_related',
                        'function': 'short'
                    }
                ],
                'rule': lambda x, y: x & (~ y)
            })
        ],
        'very_low': [
            Rule(**{
                'params': [
                    {
                        'field': 'study_related',
                        'function': 'short'
                    },
                    {
                        'field': 'performance_related',
                        'function': 'low'
                    }
                ],
                'rule': lambda x, y: x & y
            }),
            Rule(**{
                'params': [
                    {
                        'field': 'study_goal',
                        'function': 'short'
                    },
                    {
                        'field': 'performance_goal',
                        'function': 'low'
                    }
                ],
                'rule': lambda x, y: x & y
            })
        ]
    }


def mom(values, functions):
    x = np.linspace(0, 1, 1000)
    max_value = values['value'].max()

    relevant = values[np.isclose(values['value'], max_value, atol=.001)]

    def operations():
        for value in relevant:
            yield lambda var, max=value['value']: functions[value['level']](var, max)

    def operation(var):
        return max([op(var) for op in operations()])

    y = np.vectorize(operation, otypes=['f4'])(x)

    return np.mean(x[np.isclose(y, max_value, atol=.001)])


def centroid(values, functions):
    x = np.linspace(0, 1, 1000)

    def operations():
        for value in values:
            yield lambda var, max=value['value']: functions[value['level']](var, max)

    def operation(var):
        return max([op(var) for op in operations()])

    y = np.vectorize(operation, otypes=['f4'])(x)

    return (x * y).sum() / y.sum()


solution_functions = {
    'mom': mom,
    'centroid': centroid
}


def defuzzify(values, functions, method):
    return solution_functions[method](values, functions)


def normalize(data: np.ndarray, c=1.) -> np.ndarray:
    for name in data.dtype.names:
        if data.dtype[name] == np.dtype('f4'):
            max = data[name].max()
            min = data[name].min()

            data[name] = c * ((data[name] - min) / (max - min))

    return data


def plot_functions(names, title):
    out_functions = functions['knowledge_level']

    plt.figure(title)

    x = np.linspace(0, 1, 1000)

    for name in names:
        y = np.vectorize(out_functions[name], otypes=['f4'])(x)

        plt.plot(x, y, linewidth=.7, color='black')


def plot_results(value):
    out_functions = functions['knowledge_level']

    x = np.linspace(0, 1, 1000)
    y = np.vectorize(lambda x, max=value['value']: out_functions[value['level']](x, max), otypes=['f4'])(x)

    plt.fill_between(x, y, color='g', interpolate=True)


def plot_solutions(data):
    rules = build_rules()
    levels = np.unique(data['knowledge_level'])

    for line in data:
        print(line['knowledge_level'])

        plot_functions(levels, 'Knowledge levels')

        values = np.array([(max([rule.apply(line) for rule in rules[level]]), level) for level in levels],
                          dtype=[('value', 'f4'),
                                 ('level', 'U20')])

        solution = defuzzify(values, functions['knowledge_level'], 'mom')

        print(f'\tMOM: {solution:f}')

        plt.axvline(x=solution, color='c', label=f'MOM {solution:.2f}')

        solution = defuzzify(values, functions['knowledge_level'], 'centroid')

        print(f'\tCentroid: {solution:f}')

        plt.axvline(x=solution, color='m', label=f'Centroid {solution:.2f}')

        for value in values:
            plot_results(value)

        plt.legend(loc='lower right')
        plt.show()

        print()


def print_detailed_analysis(data, category):
    for type in np.unique(data[category]):
        rows = data[data[category] == type]

        print(type)

        for name in data.dtype.names:
            if data.dtype[name] == np.dtype('f4'):
                column = rows[name]

                print(name)

                min = column.min()
                mean = np.mean(column)
                max = column.max()

                for item in functions[name].items():
                    operation_name = item[0]
                    operation = item[1]

                    print(operation_name)

                    print('\tmin: {:.2f} {:.2f}'.format(min, operation(min)))
                    print('\tmean: {:.2f} {:.2f}'.format(mean, operation(mean)))
                    print('\tmax: {:.2f} {:.2f}'.format(max, operation(max)))

                print()

        print()


def print_short_analysis(data, category):
    print(*[(np.quantile(data[name], .25), np.median(data[name]), np.quantile(data[name], 0.75), name)
            for name in data.dtype.names
            if data.dtype[name] == np.dtype('f4')],
          sep='\n', end='\n\n')

    for type in np.unique(data[category]):
        values = data[data[category] == type]

        print(type)
        print(*[(values[name].min(), np.mean(values[name]), values[name].max(), name)
                for name in data.dtype.names
                if data.dtype[name] != np.dtype('U20')],
              end='\n\n', sep='\n')


def main():
    data = np.genfromtxt('new_data.txt',
                         delimiter=';',
                         dtype=[('study_goal', 'f4'),
                                ('repetition_goal', 'f4'),
                                ('study_related', 'f4'),
                                ('performance_related', 'f4'),
                                ('performance_goal', 'f4'),
                                ('knowledge_level', 'U20')])

    # data = normalize(data)

    # print_short_analysis(data, 'knowledge_level')
    # print_detailed_analysis(data, 'knowledge_level')

    plot_solutions(data)


if __name__ == '__main__':
    main()
